<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JsonSerializable;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
class Place implements JsonSerializable
{
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255)]
    private string $type;

    /**
     * @var Collection<AccountPlace> $workers
     */
    #[ORM\OneToMany(mappedBy: 'place', targetEntity: AccountPlace::class)]
    private Collection $workers;

    #[ORM\OneToOne(inversedBy: 'place', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private Address $address;

    /**
     * @var Collection<Zone> $zones
     */
    #[ORM\OneToMany(mappedBy: 'place', targetEntity: Zone::class)]
    private Collection $zones;

    #[ORM\ManyToOne(inversedBy: 'places')]
    private ?Organization $organization = null;

    public function __construct()
    {
        $this->workers = new ArrayCollection();
        $this->zones = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, AccountPlace>
     */
    public function getWorkers(): Collection
    {
        return $this->workers;
    }

    public function addWorker(AccountPlace $worker): self
    {
        if (!$this->workers->contains($worker)) {
            $this->workers->add($worker);
            $worker->setPlace($this);
        }

        return $this;
    }

    public function removeWorker(AccountPlace $worker): self
    {
        $this->workers->removeElement($worker);

        return $this;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Zone>
     */
    public function getZones(): Collection
    {
        return $this->zones;
    }

    public function addZone(Zone $zone): self
    {
        if (!$this->zones->contains($zone)) {
            $this->zones->add($zone);
            $zone->setPlace($this);
        }

        return $this;
    }

    public function removeZone(Zone $zone): self
    {
        $this->zones->removeElement($zone);

        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'address' => $this->prepareAddress(),
            'workers' => $this->prepareWorkers()
        ];
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return array<string, int|string|null>
     */
    private function prepareAddress(): array
    {
        return [
            'id' => $this->getAddress()->getId(),
            'postCode' => $this->getAddress()->getPostCode(),
            'city' => $this->getAddress()->getCity(),
            'street' => $this->getAddress()->getStreet(),
            'country' => $this->getAddress()->getCountry(),
        ];
    }

//    /**
//     * @return array<int, array<string, array<string, mixed>|bool|int|string|null>>
//     */
//    private function prepareZones(): array
//    {
//        $zones = [];
//        /** @var Zone $zone */
//        foreach ($this->getZones() as $zone) {
//            $zones[] = [
//                'id' => $zone->getId(),
//                'name' => $zone->getName(),
//            ];
//        }
//
//        return $zones;
//    }

    /**
     * @return array<int, array<string, Account|int|string|null>>
     */
    private function prepareWorkers(): array
    {
        $workers = [];
        /** @var AccountPlace $worker */
        foreach ($this->getWorkers() as $worker) {
            $workers[] = [
                'id' => $worker->getId(),
                'email' => $worker->getAccount()->getEmail(),
                'username' => $worker->getAccount()->getUsername(),
                'employedAs' => $worker->getEmployedAs(),
            ];
        }

        return $workers;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
