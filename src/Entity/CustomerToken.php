<?php

namespace App\Entity;

use App\Repository\CustomerTokenRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use JsonSerializable;

#[ORM\Entity(repositoryClass: CustomerTokenRepository::class)]
class CustomerToken implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'tokens')]
    private ?Customer $customer = null;

    #[ORM\Column(length: 255)]
    private string $token;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $validTo;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $usedAt = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): CustomerToken
    {
        $this->token = $token;

        return $this;
    }

    public function getValidTo(): ?\DateTimeInterface
    {
        return $this->validTo;
    }

    public function setValidTo(\DateTimeInterface $validTo): self
    {
        $this->validTo = \DateTimeImmutable::createFromInterface($validTo);

        return $this;
    }

    public function getUsedAt(): ?\DateTimeImmutable
    {
        return $this->usedAt;
    }

    public function setUsedAt(?\DateTimeImmutable $usedAt): self
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'validTo' => $this->getValidTo(),
            'usedAt' => $this->getUsedAt(),
            'customer' => $this->prepareCustomer(),
        ];
    }

    /**
     * @return array<string, int|string|null>
     */
    private function prepareCustomer(): array
    {
        return [
            'id' => $this->getCustomer()?->getId(),
            'name' => $this->getCustomer()?->getName(),
            'lastName' => $this->getCustomer()?->getLastName(),
            'phone' => $this->getCustomer()?->getPhone(),
            'email' => $this->getCustomer()?->getEmail(),
        ];
    }
}
