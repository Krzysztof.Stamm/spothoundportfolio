<?php

namespace App\Entity;

use App\Repository\AccountPlaceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use JsonSerializable;

#[ORM\Entity(repositoryClass: AccountPlaceRepository::class)]
class AccountPlace implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'workers')]
    #[ORM\JoinColumn(nullable: false)]
    private Place $place;

    #[ORM\ManyToOne(inversedBy: 'worksAt')]
    #[ORM\JoinColumn(nullable: false)]
    private Account $account;

    #[ORM\Column(length: 255)]
    private string $employedAs;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function setPlace(Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getEmployedAs(): string
    {
        return $this->employedAs;
    }

    public function setEmployedAs(string $employedAs): self
    {
        $this->employedAs = $employedAs;

        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'employedAs' => $this->getEmployedAs(),
            'place' => $this->preparePlace(),

        ];
    }

    /**
     * @return array<string, Address|int|string|null>
     */
    private function preparePlace(): array
    {
        return [
            'id' => $this->getPlace()->getId(),
            'name' => $this->getPlace()->getName(),
            'address' => $this->getPlace()->getAddress(),
            'type' => $this->getPlace()->getType(),
        ];
    }
}
