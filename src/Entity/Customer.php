<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JsonSerializable;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer implements JsonSerializable
{
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255)]
    private string $lastName;

    #[ORM\Column(length: 32)]
    private string $phone;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    /**
     * @var Collection<CustomerToken> $tokens
     */
    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: CustomerToken::class)]
    private Collection $tokens;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, CustomerToken>
     */
    public function getTokens(): Collection
    {
        return $this->tokens;
    }

    public function addToken(CustomerToken $token): self
    {
        if (!$this->tokens->contains($token)) {
            $this->tokens->add($token);
            $token->setCustomer($this);
        }

        return $this;
    }

    public function removeToken(CustomerToken $token): self
    {
        if ($this->tokens->removeElement($token)) {
            if ($token->getCustomer() === $this) {
                $token->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return array{id: string|null, name: string|null, lastName: string|null, phone: string, email: string|null, tokens: array<int, array{id: string|null, validTo: DateTimeInterface|null, usedAt: DateTimeInterface|null}>}
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'lastName' => $this->getLastName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'tokens' => $this->prepareTokens(),
        ];
    }

    /**
     * @return array<int, array{id: string|null, validTo: DateTimeInterface|null, usedAt: DateTimeInterface|null}>
     */
    private function prepareTokens(): array
    {
        $tokens = [];

        foreach ($this->getTokens() as $token) {
            $tokens[] = [
                'id' => $token->getId(),
                'validTo' => $token->getValidTo(),
                'usedAt' => $token->getUsedAt()
            ];
        }

        return $tokens;
    }
}
