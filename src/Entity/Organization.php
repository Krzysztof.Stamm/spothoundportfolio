<?php

namespace App\Entity;

use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use JsonSerializable;

#[ORM\Entity(repositoryClass: OrganizationRepository::class)]
class Organization implements JsonSerializable
{
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\OneToOne(inversedBy: 'ownedOrganization', cascade: ['persist', 'remove'])]
    private ?Account $owner = null;

    /**
     * @var Collection<Account> $employee
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: Account::class)]
    private Collection $employee;

    #[ORM\Column(length: 10)]
    private string $alias;

    /**
     * @var Collection<Place> $places
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: Place::class)]
    private Collection $places;

    public function __construct()
    {
        $this->employee = new ArrayCollection();
        $this->places = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOwner(): ?Account
    {
        return $this->owner;
    }

    public function setOwner(?Account $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, Account>
     */
    public function getEmployees(): Collection
    {
        return $this->employee;
    }

    public function addEmployee(Account $employee): self
    {
        if (!$this->employee->contains($employee)) {
            $this->employee->add($employee);
            $employee->setOrganization($this);
        }

        return $this;
    }

    public function removeEmployee(Account $employee): self
    {
        if ($this->employee->removeElement($employee)) {
            if ($employee->getOrganization() === $this) {
                $employee->setOrganization(null);
            }
        }

        return $this;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return array<string, array<string, int|string|null>|int|string|null>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'owner' => $this->prepareOrganizationOwner(),
        ];
    }

    /**
     * @return Collection<int, Place>
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places->add($place);
            $place->setOrganization($this);
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            if ($place->getOrganization() === $this) {
                $place->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return array<string, int|string|null>
     */
    private function prepareOrganizationOwner(): array
    {
        return [
            'id' => $this->getOwner()?->getId(),
            'email' => $this->getOwner()?->getEmail(),
        ];
    }
}
