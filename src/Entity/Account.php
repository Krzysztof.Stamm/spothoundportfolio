<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Ignore;
use JsonSerializable;

#[ORM\Entity(repositoryClass: AccountRepository::class)]
class Account implements UserInterface, PasswordAuthenticatedUserInterface, JsonSerializable
{
    use BlameableEntity;
    use TimestampableEntity;
//    use SoftDeleteableEntity;

    public const TYPE_OWNER = 'TYPE_OWNER';

    public const TYPE_EMPLOYEE = 'TYPE_EMPLOYEE';

    const ACCOUNT_TYPES = [
      self::TYPE_EMPLOYEE => self::TYPE_EMPLOYEE,
      self::TYPE_OWNER => self::TYPE_OWNER
    ];

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?string $id = null;

    #[ORM\Column(length: 180, unique: true, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 50, unique: true)]
    private string $username;

    #[ORM\Column(length: 100)]
    private string $firstname;

    #[ORM\Column(length: 100)]
    private string $lastname;

    /**
     * @var array<int, string> $roles
     */
    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column]
    #[Ignore]
    private string $password;

    #[ORM\ManyToOne(inversedBy: 'Employee')]
    private ?Organization $organization = null;

    #[ORM\Column(length: 50)]
    private string $accountType;

    #[ORM\ManyToOne]
    private ?Place $placeContext = null;

    #[ORM\OneToOne(mappedBy: 'owner', cascade: ['persist', 'remove'])]
    private ?Organization $ownedOrganization = null;

    /**
     * @var Collection<AccountPlace> $worksAt
     */
    #[ORM\OneToMany(mappedBy: 'account', targetEntity: AccountPlace::class)]
    private Collection $worksAt;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $isVerified = true;

    public function __construct()
    {
        $this->worksAt = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array<int, string> $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFullName(): string
    {
        return sprintf('%s %s', $this->getFirstname(), $this->getLastname());
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getOwnedOrganization(): ?Organization
    {
        return $this->ownedOrganization;
    }

    public function setOwnedOrganization(?Organization $ownedOrganization): self
    {
        // unset the owning side of the relation if necessary
        if ($ownedOrganization === null && $this->ownedOrganization !== null) {
            $this->ownedOrganization->setOwner(null);
        }

        // set the owning side of the relation if necessary
        if ($ownedOrganization !== null && $ownedOrganization->getOwner() !== $this) {
            $ownedOrganization->setOwner($this);
        }

        $this->ownedOrganization = $ownedOrganization;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return Collection<int, AccountPlace>
     */
    public function getWorksAt(): Collection
    {
        return $this->worksAt;
    }

    public function addWorksAt(AccountPlace $worksAt): self
    {
        if (!$this->worksAt->contains($worksAt)) {
            $this->worksAt->add($worksAt);
            $worksAt->setAccount($this);
        }

        return $this;
    }

    public function removeWorksAt(AccountPlace $worksAt): self
    {
        $this->worksAt->removeElement($worksAt);

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getPlaceContext(): ?Place
    {
        return $this->placeContext;
    }

    public function setPlaceContext(?Place $placeContext): self
    {
        $this->placeContext = $placeContext;

        return $this;
    }

    public function getAccountType(): string
    {
        return $this->accountType;
    }

    public function setAccountType(string $accountType): self
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * @return array<string, Organization|array<string, int|string|null>|int|string|null>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'organization' => $this->prepareOrganization(),
            'ownedOrganization' => $this->prepareOwnedOrganization(),
        ];
    }

    /**
     * @return array<string, int|string|null>
     */
    private function prepareOrganization(): array
    {
        return [
            'Id' => $this->getOrganization()?->getId(),
            'name' => $this->getOrganization()?->getName(),
        ];
    }

    /**
     * @return array<string, int|string|null>
     */
    private function prepareOwnedOrganization(): array
    {
        return [
            'Id' => $this->getOwnedOrganization()?->getId(),
            'name' => $this->getOwnedOrganization()?->getName(),
        ];
    }
}
