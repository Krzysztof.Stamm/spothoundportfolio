<?php

namespace App\Twig;

use App\Enum\ReservationStatus;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('StatusToText', [$this, 'statusToTextFilter']),
        ];
    }

    public function statusToTextFilter(int $status): string
    {
        return ReservationStatus::fromValue($status);
    }
}
