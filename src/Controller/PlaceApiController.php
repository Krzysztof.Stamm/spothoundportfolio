<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\AccountPlace;
use App\Entity\Address;
use App\Entity\Place;
use App\Repository\PlaceRepository;
use App\Repository\ReservationRepository;
use App\Repository\ZoneRepository;
use App\Service\ContextJWTService;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes\Tag;
use OpenApi\Annotations as OA;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/place', name: 'api_place_'),Tag(name:'Place'),]
class PlaceApiController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get place by JWT place context",
     *     description="Returns a place",
     *     @OA\Response(
     *         response=200,
     *         description="The place",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="name", type="string", example="Najlepsza restauracja"),
     *             @OA\Property(property="type", type="string", example="Restauracja"),
     *             @OA\Property(
     *                 property="address",
     *                 type="object",
     *                 @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                 @OA\Property(property="postCode", type="string", example="62-065"),
     *                 @OA\Property(property="city", type="string", example="Grodzisk Wielkopolski"),
     *                 @OA\Property(property="street", type="string", example="Majkowskich 13"),
     *                 @OA\Property(property="country", type="string", example="Polska")
     *             ),
     *             @OA\Property(
     *                 property="workers",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                     @OA\Property(property="email", type="string", example="foo@bar.pl"),
     *                     @OA\Property(property="username", type="string", example="kwrobinski"),
     *                     @OA\Property(property="employedAs", type="string", example="Admin")
     *                 )
     *             ),
     *             @OA\Property(
     *                 property="zones",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                     @OA\Property(property="name", type="string", example="Ogródek")
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Place not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param ContextJWTService $contextJWTService
     */
    public function index(
        ContextJWTService $contextJWTService
    ): JsonResponse {
        $place = $contextJWTService->getPlaceContext();

        /** @var Account $user */
        $user = $this->getUser();

        if (null === $place || $user->getOrganization()?->getId() !== $place->getOrganization()?->getId()) {
            return new JsonResponse(['error' => 'Place doesn\'t exist.'], 400);
        }

        return $this->json($place);
    }

    /**
     * @Route("/", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Add new place",
     *     description="Adds a new place",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the new place",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Moje miejsce"),
     *             @OA\Property(property="type", type="string", example="Kawiarnia"),
     *             @OA\Property(property="postCode", type="string", example="00-001"),
     *             @OA\Property(property="city", type="string", example="Warszawa"),
     *             @OA\Property(property="street", type="string", example="Nowy Świat 1"),
     *             @OA\Property(property="country", type="string", example="Polska")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="The newly created place",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request - Place with the given address already exists"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function addPlace(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $address = $em->getRepository(Address::class)->findOneBy([
            'postCode' => $data['postCode'],
            'city' => $data['city'],
            'street' => $data['street'],
        ]);

        if ($address !== null) {
            return new JsonResponse(['error' => 'Place with the given address already exists'], 400);
        }

        $account = $this->getUser();
        assert($account instanceof Account);

        $address = new Address();
        $address
            ->setPostCode($data['postCode'])
            ->setCity($data['city'])
            ->setStreet($data['street'])
            ->setCountry($data['country']);

        $em->persist($address);

        $place = new Place();
        $place
            ->setName($data['name'])
            ->setType($data['type'])
            ->setOrganization($account->getOrganization())
            ->setAddress($address);

        $em->persist($place);

        $accountPlace = new AccountPlace();
        $accountPlace
            ->setPlace($place)
            ->setAccount($account)
            ->setEmployedAs(Account::TYPE_OWNER);

        $em->persist($accountPlace);

        $em->flush();

        return new JsonResponse(['id' => $place->getId()], 201);
    }

    /**
     * @Route("/zones", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get Zones defined for Place",
     *     description="Returns place zones",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                 @OA\Property(property="name", type="string", example="ZoneExample"),
     *                 @OA\Property(property="position", type="integer", example="223"),
     *                 @OA\Property(property="active", type="boolean", example="true"),
     *                 @OA\Property(
     *                     property="configuration",
     *                     oneOf={
     *                         @OA\Schema(
     *                             type="object",
     *                             @OA\Property(property="color", type="string", example="Green")
     *                         ),
     *                         @OA\Schema(
     *                             type="array",
     *                             @OA\Items(type="string")
     *                         )
     *                     }
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param ZoneRepository $zoneRepository
     * @param ContextJWTService $contextJWTService
     */
    public function getPlaceZones(
        ZoneRepository $zoneRepository,
        ContextJWTService $contextJWTService
    ): JsonResponse {
        $place = $contextJWTService->getPlaceContext();

        /** @var Account $user */
        $user = $this->getUser();

        if (null === $place || $user->getOrganization()?->getId() !== $place->getOrganization()?->getId()) {
            return new JsonResponse(['error' => 'Place doesn\'t exist.'], 400);
        }

        return $this->json($zoneRepository->getZonesForPlace($place));
    }

    /**
     * @Route("", methods={"PUT"})
     *
     * @OA\Put(
     *     path="/api/place",
     *     summary="Updates a place.",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the updated place",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="My place"),
     *             @OA\Property(property="type", type="string", example="Restaurant"),
     *             @OA\Property(
     *                 property="address",
     *                 type="object",
     *                 @OA\Property(property="postcode", type="string", example="12345"),
     *                 @OA\Property(property="city", type="string", example="New York"),
     *                 @OA\Property(property="street", type="string", example="Main Street"),
     *                 @OA\Property(property="country", type="string", example="USA")
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Place updated successfully.",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid request data.",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized access.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Place not found.",
     *     )
     * )
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ContextJWTService $contextJWTService
     */
    public function updatePlace(
        Request $request,
        EntityManagerInterface $entityManager,
        ContextJWTService $contextJWTService,
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $place = $contextJWTService->getPlaceContext();

        /** @var Account $user */
        $user = $this->getUser();

        if (null === $place || $user->getOrganization()?->getId() !== $place->getOrganization()?->getId()) {
            return new JsonResponse(['error' => 'Place doesn\'t exist.'], 400);
        }

        if (isset($data['name'])) {
            $place->setName($data['name']);
        }

        if (isset($data['type'])) {
            $place->setType($data['type']);
        }

        if (isset($data['address'])) {
            $address = $place->getAddress();

            if (isset($data['address']['postcode'])) {
                $address->setPostCode($data['address']['postcode']);
            }

            if (isset($data['address']['city'])) {
                $address->setCity($data['address']['city']);
            }

            if (isset($data['address']['street'])) {
                $address->setStreet($data['address']['street']);
            }

            if (isset($data['address']['country'])) {
                $address->setCountry($data['address']['country']);
            }
        }

        $entityManager->flush();

        return new JsonResponse(['message' => sprintf('Place "%s" updated successfully.', $place)]);
    }

    /**
     * @Route("/statistics", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get place statistics",
     *     description="Returns JSON containing place statistics",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="node_count", type="integer", example="1"),
     *                 @OA\Property(property="user_count", type="integer", example="4"),
     *                 @OA\Property(property="reservations_24h", type="integer", example="88")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param PlaceRepository $placeRepository
     * @param ContextJWTService $contextJWTService
     * @param ReservationRepository $reservationRepository
     */
    public function getStatistics(
        PlaceRepository $placeRepository,
        ContextJWTService $contextJWTService,
        ReservationRepository $reservationRepository,
    ): JsonResponse {
        $place = $contextJWTService->getPlaceContext();
        $result = [];

        if (null === $place) {
            return $this->json($result);
        }

        $now = new \DateTimeImmutable();

        $result['node_count'] = $placeRepository->getNodeCount($place);
        $result['user_count'] = count($place->getWorkers());
        $result['reservations_24h'] = count($reservationRepository->getReservationsForPlace(
            $place,
            $now->setTime(0, 0),
            $now->setTime(23, 59, 59)
        ));

        return $this->json($result);
    }
}
