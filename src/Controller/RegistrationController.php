<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Organization;
use App\Form\RegistrationFormType;
use App\Repository\AccountRepository;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes\Tag;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use OpenApi\Annotations as OA;

#[Route('/api', name: 'api_'), Tag(name:'Util')]
class RegistrationController extends AbstractController
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/register", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Register an account",
     *     description="Register an account",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the new account",
     *         @OA\JsonContent(
     *             @OA\Property(property="agreeTerms", type="boolean", example="true"),
     *             @OA\Property(property="email", type="string", example="foo@bar.pl"),
     *             @OA\Property(property="password", type="string", example="strongpassword1"),
     *             @OA\Property(property="organizationName", type="string", example="Cool organization"),
     *             @OA\Property(property="firstname", type="string", example="Foo"),
     *             @OA\Property(property="lastname", type="string", example="Bar"),
     *             @OA\Property(property="username", type="string", example="Barname"),
     *             @OA\Property(property="alias", type="string", example="BDR12")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="The newly created account",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request - Account with the given address already exists"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $account = new Account();
        $data = json_decode($request->getContent(), true);

        if (isset($data['password'])) {
            $data['plainPassword'] = $data['password'];
            unset($data['password']);
        }

        $form = $this->createForm(RegistrationFormType::class, $account);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $account->setPassword(
                $userPasswordHasher->hashPassword(
                    $account,
                    $form->get('plainPassword')->getData()
                )
            );
            $account->setRoles(['ROLE_USER']);

            $entityManager->persist($account);

            $organization = new Organization();
            $organization
                ->setOwner($account)
                ->setAlias($data['alias'])
                ->setName($data['organizationName']);

            $account->setOrganization($organization);
            $entityManager->persist($organization);
            $entityManager->flush();

            assert(null !== $account->getEmail());
            $this->emailVerifier->sendEmailConfirmation(
                'api_verify_email',
                $account,
                (new TemplatedEmail())
                    ->from(new Address('contact.spothound@gmail.com', 'Spothound'))
                    ->to($account->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );

            return $this->json(['message' => 'ok']);
        }

        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $errors[] = $error->getMessage();
        }

        return $this->json(['errors' => $errors], 400);
    }

    #[Route('/verify/email', name: 'verify_email', methods: ['GET'])]
    public function verifyUserEmail(Request $request, TranslatorInterface $translator, AccountRepository $accountRepository): Response
    {
        $id = $request->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $account = $accountRepository->find($id);

        if (null === $account) {
            return $this->redirectToRoute('app_register');
        }

        try {
            $this->emailVerifier->handleEmailConfirmation($request, $account);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('app_register');
        }

        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirect('/');
    }
}
