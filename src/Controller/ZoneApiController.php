<?php

namespace App\Controller;

use App\Entity\Node;
use App\Entity\Place;
use App\Entity\Zone;
use App\Repository\NodeRepository;
use App\Service\ContextJWTService;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/zone', name: 'api_zone_'), Tag(name:'Zone')]
class ZoneApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get zone by ID",
     *     description="Returns zone details by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the zone",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="name", type="string", example="ExampleZone"),
     *             @OA\Property(property="active", type="boolean", example="true"),
     *             @OA\Property(property="place", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="Moja Knajpa")
     *             ),
     *             @OA\Property(
     *                 property="configuration",
     *                 oneOf={
     *                     @OA\Schema(
     *                         type="object",
     *                         @OA\Property(property="color", type="string", example="Green")
     *                     ),
     *                     @OA\Schema(
     *                         type="array",
     *                         @OA\Items(type="string")
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Zone not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Zone $zone
     */
    public function index(Zone $zone): JsonResponse
    {
        return $this->json($zone);
    }

    /**
     * @Route("", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Add new zone",
     *     description="Adds a new zone",
     *     @OA\RequestBody(
     *         description="JSON object containing data for the new zone",
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Strefa 1"),
     *             @OA\Property(property="position", type="integer", example=14),
     *             @OA\Property(
     *                 property="configuration",
     *                 type="object",
     *                 @OA\AdditionalProperties(
     *                     type="string",
     *                     example="value1"
     *                 ),
     *                 example={"key1": "value1", "key2": "value2"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="The newly created zone",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request - Zone with the given name and position for place X exists",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ContextJWTService $contextJWTService
     */
    public function addZone(Request $request, EntityManagerInterface $em, ContextJWTService $contextJWTService): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $place = $contextJWTService->getPlaceContext();
        assert($place instanceof Place);

        $zone = $em->getRepository(Zone::class)->findOneBy([
            'name' => $data['name'],
            'position' => $data['position'],
            'place' => $place,
        ]);

        if ($zone !== null) {
            return new JsonResponse(['error' => sprintf('Zone with the given name and position for place "%s" exists', $place)], 400);
        }

        $zone = (new Zone())
            ->setName($data['name'])
            ->setPosition($data['position'])
            ->setPlace($place)
            ->setActive(true)
            ->setConfiguration($data['configuration']);

        $em->persist($zone);
        $em->flush();

        return new JsonResponse(['id' => $zone->getId()], 201);
    }

    /**
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Put(
     *     path="/api/zone/{id}",
     *     summary="Updates a zone.",
     *     tags={"Zone"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the zone.",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         description="JSON object containing data for the updated zone",
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Moje miejsce"),
     *             @OA\Property(property="position", type="int", example="223"),
     *             @OA\Property(property="active", type="boolean", example=true),
     *             @OA\Property(
     *                 property="configuration",
     *                 type="object",
     *                 @OA\AdditionalProperties(
     *                     type="string",
     *                     example="value"
     *                 )
     *             ),
     *             @OA\Property(
     *                 property="nodes",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     required={"name", "configuration"},
     *                     @OA\Property(property="name", type="string", example="Node 1"),
     *                     @OA\Property(property="size", type="int", example="4"),
     *                     @OA\Property(
     *                         property="configuration",
     *                         type="object",
     *                         @OA\AdditionalProperties(
     *                             type="string",
     *                             example="value"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Zone updated successfully.",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid request data.",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized access.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Zone not found.",
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Zone $zone
     */
    public function updateZone(Request $request, EntityManagerInterface $entityManager, Zone $zone): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (isset($data['name'])) {
            $zone->setName($data['name']);
        }

        if (isset($data['position'])) {
            $zone->setPosition($data['position']);
        }

        if (isset($data['active'])) {
            $zone->setActive($data['active']);
        }

        if (isset($data['configuration'])) {
            $zone->setConfiguration($data['configuration']);
        }

        if (isset($data['nodes'])) {
            $resultSet = [];

            foreach ($data['nodes'] as $nodeData) {
                if (!isset($nodeData['name']) || !isset($nodeData['configuration']) || !isset($nodeData['size'])) {
                    return new JsonResponse(['error' => 'Node data is invalid.'], 400);
                }

                $node = !empty($nodeData['id']) ? $entityManager->getReference(Node::class, $nodeData['id']) : new Node();

                $node
                    ->setName($nodeData['name'])
                    ->setConfiguration($nodeData['configuration'])
                    ->setZone($zone)
                    ->setSize($nodeData['size']);

                $entityManager->persist($node);
                $resultSet[] = $node;
            }
            $entityManager->flush();

            foreach ($zone->getNodes() as $node) {
                if (!in_array($node, $resultSet)) {
                    $zone->removeNode($node);
                }
            }

            $entityManager->flush();
        }

        return new JsonResponse(['message' => sprintf('Zone "%s" updated successfully.', $zone)]);
    }

    /**
     * @Route("/{id}/nodes", methods={"GET"})
     * @OA\Get(
     *     summary="Get Zone nodes",
     *     description="Returns zone nodes",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the zone",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="zone", type="string", example="Zone1"),
     *                 @OA\Property(property="name", type="string", example="Chair"),
     *                 @OA\Property(property="size", type="int", example="4"),
     *                 @OA\Property(
     *                     property="configuration",
     *                     oneOf={
     *                         @OA\Schema(
     *                             type="object",
     *                             @OA\Property(property="color", type="string", example="Green")
     *                         ),
     *                         @OA\Schema(
     *                             type="array",
     *                             @OA\Items(type="string")
     *                         )
     *                     }
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Zone not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Zone $zone
     * @param NodeRepository $nodeRepository
     */
    public function getZoneNodes(Zone $zone, NodeRepository $nodeRepository): JsonResponse
    {
        return $this->json($nodeRepository->getNodesForZone($zone));
    }
}
