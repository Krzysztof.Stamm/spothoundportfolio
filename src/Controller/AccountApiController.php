<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\AccountPlace;
use App\Entity\Place;
use App\Form\AccountCreateFormType;
use App\Repository\AccountPlaceRepository;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/account', name: 'api_account_'),Tag(name: 'Account')]
class AccountApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get account by ID",
     *     description="Returns account details by ID ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the account",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="email", type="string", example="example@wp.pl"),
     *             @OA\Property(property="organization", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="Wielka organizacja")),
     *             @OA\Property(property="ownedOrganization", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="Wielka organizacja")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Account not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Account $account
     */
    public function index(Account $account): JsonResponse
    {
        return $this->json($account);
    }

    /**
     * @Route("/{id}/places", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get places for an account by ID",
     *     description="Returns places",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the account",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="List of places for the account",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                 @OA\Property(property="employedAs", type="string", example="admin"),
     *                 @OA\Property(property="name", type="string", example="Moja Knajpa"),
     *                 @OA\Property(property="type", type="string", example="Restauracja"),
     *                 @OA\Property(property="street", type="string", example="Mickiewicza"),
     *                 @OA\Property(property="city", type="string", example="Grodzisk Wielkopolski"),
     *                 @OA\Property(property="postCode", type="string", example="62-065"),
     *                 @OA\Property(property="country", type="string", example="Polska")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Account not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Account $account
     * @param AccountPlaceRepository $accountPlaceRepository
     */
    public function getUserPlaces(Account $account, AccountPlaceRepository $accountPlaceRepository): JsonResponse
    {
        $places = $accountPlaceRepository->getPlacesForAccount($account);

        return $this->json($places);
    }

    /**
     * @Route("/", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Add new account",
     *     description="Adds a new account",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the new account",
     *         @OA\JsonContent(
     *             @OA\Property(property="username", type="string", example="employee1"),
     *             @OA\Property(property="placeContext", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="email", type="string", example="foo@bar.pl"),
     *             @OA\Property(property="password", type="string", example="strongpassword1"),
     *             @OA\Property(property="placeId", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="firstname", type="string", example="Foo"),
     *             @OA\Property(property="lastname", type="string", example="Bar"),
     *             @OA\Property(property="employedAs", type="string", example="Manager")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="The newly created account",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request - Account with the given address already exists"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param PlaceRepository $placeRepository
     */
    public function addAccount(Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $userPasswordHasher, PlaceRepository $placeRepository): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $loggedAccount = $this->getUser();
        assert($loggedAccount instanceof Account);

        if (isset($data['password'])) {
            $data['plainPassword'] = $data['password'];
            unset($data['password']);
        }
        $account = new Account();
        $form = $this->createForm(AccountCreateFormType::class, $account);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $placeContext = $em->getReference(Place::class, $data['placeContext'] ?? '');
            $account->setPassword(
                $userPasswordHasher->hashPassword(
                    $account,
                    $form->get('plainPassword')->getData()
                )
            )
                ->setRoles(['ROLE_USER'])
                ->setPlaceContext($placeContext)
                ->setIsVerified(true)
                ->setEmail($data['email'])
                ->setOrganization($loggedAccount->getOrganization())
                ->setFirstName($data['firstname'])
                ->setLastName($data['lastname'])
                ->setAccountType(Account::TYPE_EMPLOYEE)
                ->setUsername($data['username']);
            $place = $em->getReference(Place::class, $data['placeId']);
            assert($place instanceof Place);
            $accountPlace = (new AccountPlace())
                ->setEmployedAs($data['employedAs'])
                ->setAccount($account)
                ->setPlace($place);

            $em->persist($accountPlace);

            $em->persist($account);
            $em->flush();

            return new JsonResponse(['id' => $account->getId()], 201);
        }

        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $errors[] = $error->getMessage();
        }

        return $this->json(['errors' => $errors], 400);
    }

    /**
     * @Route("/context", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Set place context to account",
     *     description="Sets place context to account",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for sets place context to account",
     *         @OA\JsonContent(
     *             @OA\Property(property="placeId", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *         )
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Successfully set place context"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Bad Request - Place doesn't exist"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param PlaceRepository $placeRepository
     * @param EntityManagerInterface $em
     */
    public function setContext(Request $request, PlaceRepository $placeRepository, EntityManagerInterface $em): JsonResponse
    {
        /** @var Account $user */
        $user = $this->getUser();

        $data = json_decode($request->getContent(), true);
        $place = $placeRepository->find($data['placeId'] ?? '');

        if (null === $place || $place->getOrganization() !== $user->getOrganization()) {
            return new JsonResponse(['error' => 'Place doesn\'t exist.'], 400);
        }

        $user->setPlaceContext($place);
        $em->flush();

        return $this->json(['message' => 'Successfully set place context']);
    }

    /**
     * @Route("/assign", methods={"PUT"})
     *
     * @OA\Put(
     *     path="/api/account/assign",
     *     summary="Assign account to place.",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing IDs for the place and account",
     *         @OA\JsonContent(
     *             @OA\Property(property="placeId", type="string", example="0187f73f-ea81-7764-a2f1-3a46075d95d3"),
     *             @OA\Property(property="accountId", type="string", example="0187f73f-1232-7764-a2f1-3a46075d95d3")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Account assigned successfully."
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Can't assign account to this place."
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="Account is already assigned to this place."
     *     )
     * )
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param AccountPlaceRepository $accountPlaceRepository
     */
    public function assign(
        Request $request,
        EntityManagerInterface $em,
        AccountPlaceRepository $accountPlaceRepository
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);
        $account = $em->getReference(Account::class, $data['accountId']);
        $place = $em->getReference(Place::class, $data['placeId']);
        $accountPlace = $accountPlaceRepository->findOneBy(['account' => $account, 'place' => $place]);

        if ($accountPlace) {
            return $this->json(['message' => 'Account is already assigned to this place.'], 409);
        }

        try {
            assert($account instanceof Account);
            assert($place instanceof Place);
            $accountPlace = (new AccountPlace())
                ->setEmployedAs(Account::TYPE_EMPLOYEE)
                ->setAccount($account)
                ->setPlace($place);

            $em->persist($accountPlace);
            $em->flush();

            return $this->json(['message' => 'Account assigned successfully.']);
        } catch (\Exception) {
            return $this->json(['message' => "Can't assign account to this place."], 400);
        }
    }
}
