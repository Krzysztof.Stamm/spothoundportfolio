<?php

namespace App\Controller;

use App\Entity\Address;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/address', name: 'api_address_'),Tag(name:'Address')]
class AddressApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get address by ID",
     *     description="Returns address details by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the address",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="postCode", type="string", example="62-065"),
     *             @OA\Property(property="city", type="string", example="Grodzisk Wielkopolski"),
     *             @OA\Property(property="street", type="string", example="Mickiewicza"),
     *             @OA\Property(property="country", type="string", example="Polska"),
     *             @OA\Property(property="place", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="Moja Knajpa")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Address $address
     */
    public function index(Address $address): JsonResponse
    {
        return $this->json($address);
    }
}
