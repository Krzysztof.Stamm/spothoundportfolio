<?php

namespace App\Controller;

use App\Entity\Node;
use App\Entity\Reservation;
use App\Enum\ReservationStatus;
use App\Repository\ReservationRepository;
use App\Service\ContextJWTService;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/reservation', name: 'api_reservation_'),Tag(name:'Reservation'),]
class ReservationApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get reservation by ID",
     *     description="Returns reservation details by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the reservation",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="status", type="string", example="Confirmed"),
     *             @OA\Property(property="dateFrom", type="string", example="2023-01-01 16:50:00"),
     *             @OA\Property(property="dateTo", type="string", example="2023-01-01 19:50:00"),
     *             @OA\Property(property="note", type="string", example="Client says that he want free onion rings"),
     *             @OA\Property(property="node", type="object",
     *                  @OA\Property(property="id", type="string", example="187f74a-df89-7b79-b7ff-089836f92c8c"),
     *                  @OA\Property(property="name", type="string", example="Node3"),
     *                  @OA\Property(property="zone", type="object",
     *                      @OA\Property(property="id", type="string", example="187f74a-df89-7b79-b7ff-089836f92c8c"),
     *                      @OA\Property(property="name", type="string", example="Ogródek")
     *                  ),
     *                  @OA\Property(property="size", type="integer", example="4"),
     *                  @OA\Property(property="configuration", type="object",
     *                      @OA\Property(property="additionalProp1", type="string", example="value1"),
     *                      @OA\Property(property="additionalProp2", type="string", example="value2"),
     *                      @OA\Property(property="additionalProp3", type="string", example="value3")
     *             )
     *             ),
     *             @OA\Property(property="customerDetails", type="string", example="John Cena")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Reservation not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Reservation $reservation
     */
    public function index(Reservation $reservation): JsonResponse
    {
        return $this->json($reservation);
    }

    /**
     * @Route("/", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Add new reservation",
     *     description="Adds a new reservation",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the new reservation",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="RESERVED"),
     *             @OA\Property(property="dateFrom", type="string", example="2023-01-01 16:50:00"),
     *             @OA\Property(property="dateTo", type="string", example="2023-01-01 19:50:00"),
     *             @OA\Property(property="note", type="string", example="Client says that he want free onion rings"),
     *             @OA\Property(property="node", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="customerDetails", type="string", example="John cena")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="The newly created reservation",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Given dates collide with existing reservations."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ReservationRepository $reservationRepository
     */
    public function addReservation(
        Request $request,
        EntityManagerInterface $em,
        ReservationRepository $reservationRepository,
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);
        $node = $em->getReference(Node::class, $data['node']);
        $status = ReservationStatus::fromName($data['status']);

        $dateFrom = new \DateTimeImmutable($data['dateFrom']);
        $dateTo = new \DateTimeImmutable($data['dateTo']);

        if (!$node || !$status || false === $reservationRepository->checkAvailability($node, $dateFrom, $dateTo)) {
            throw new \Exception('Given dates collide with existing reservations.', 400);
        }

        $reservation = (new Reservation())
            ->setNode($node)
            ->setNote($data['note'] ?? null)
            ->setSize($data['size'] ?? 1)
            ->setCustomerDetails($data['customerDetails'])
            ->setDateFrom($dateFrom)
            ->setDateTo($dateTo)
            ->setStatus($status);

        $em->persist($reservation);
        $em->flush();

        return new JsonResponse(['id' => $reservation->getId()], 201);
    }

    /**
     * @Route("/{id}/status/{status}", methods={"PUT"})
     *
     * @OA\Put(
     *     path="/api/reservation/{id}/status/{status}",
     *     summary="Updates a reservation.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the reservation.",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Parameter(
     *         name="status",
     *         in="path",
     *         description="The new status of the reservation.",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="reservation updated successfully.",
     *     @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid reservation or status.",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized access.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="reservation not found.",
     *     )
     * )
     * @param EntityManagerInterface $em
     * @param Reservation $reservation
     * @param int $status
     */
    public function updateStatus(Reservation $reservation, EntityManagerInterface $em, int $status): JsonResponse
    {
        $choices = ReservationStatus::getChoices();

        if (in_array($status, $choices)) {
            $reservation->setStatus($status);
            $em->flush();

            return new JsonResponse(['message' => sprintf('Reservation status "%s" updated successfully.', $reservation->getId())]);
        }

        // to nie powinien być put na update wszystkiego?

        throw new \Exception('Invalid reservation or status.', 400);
    }

    /**
     * @Route("/filter", methods={"POST"})
     *
     * @OA\Post (
     *     summary="Get reservations by filters",
     *     description="Returns reservations details by filters",
     *     @OA\RequestBody(
     *         required=true,
     *         description="JSON object containing data for the new reservation",
     *         @OA\JsonContent(
     *             @OA\Property(property="filters", type="object",
     *                  @OA\Property(property="reservation", type="string", example="018a7fed-427f-7198-92b3-71d230928e9a"),
     *                  @OA\Property(property="zone", type="string", example="0187f748-aa7a-7196-a736-f65723ecb720"),
     *                  @OA\Property(property="dateFrom", type="string", example="2023-09-12 00:00:00"),
     *                  @OA\Property(property="dateTo", type="string", example="2023-09-12 00:00:00"),
     *                  @OA\Property(property="customer", type="string", example="Jan CENA"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(type="object",
     *                 @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                 @OA\Property(property="status", type="string", example="Confirmed"),
     *                 @OA\Property(property="dateFrom", type="string", example="2023-01-01 16:50:00"),
     *                 @OA\Property(property="dateTo", type="string", example="2023-01-01 19:50:00"),
     *                 @OA\Property(property="note", type="string", example="Client says that he want free onion rings"),
     *                 @OA\Property(property="node", type="object",
     *                     @OA\Property(property="id", type="string", example="187f74a-df89-7b79-b7ff-089836f92c8c"),
     *                     @OA\Property(property="name", type="string", example="Node3"),
     *                         @OA\Property(property="zone", type="object",
     *                         @OA\Property(property="id", type="string", example="187f74a-df89-7b79-b7ff-089836f92c8c"),
     *                         @OA\Property(property="name", type="string", example="Ogródek")
     *                      ),
     *                     @OA\Property(property="size", type="int", example="4"),
     *                     @OA\Property(property="configuration", type="object",
     *                         @OA\Property(property="additionalProp1", type="string", example="value1"),
     *                         @OA\Property(property="additionalProp2", type="string", example="value2"),
     *                         @OA\Property(property="additionalProp3", type="string", example="value3")
     *                     )
     *                 ),
     *                 @OA\Property(property="customerDetails", type="string", example="Jan Cena")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Reservation not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Request $request
     * @param ContextJWTService $contextJWTService
     * @param ReservationRepository $reservationRepository
     */
    public function getReservationsByFilters(
        Request $request,
        ReservationRepository $reservationRepository,
        ContextJWTService $contextJWTService,
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);

        if (empty($data['filters'])) {
            return $this->json([]);
        }

        // zawsze filtr po place, bez place miałoby sens tylko dla globalnych statystyk,
        // a to na pewno nie działoby się tutaj i w taki sposób xD
        $data['filters']['place'] = $contextJWTService->getPlaceContext();

        // dateFrom i dateTo zawsze będą odwoływać się do konkretnego dnia - mamy przedział godzinowy, nie datowy
        // dateTo bez dateFrom nie ma sensu xd ALE dateFrom bez dateTo już ma i wyciągnie od from do końca dnia
        // poza tym każdy inny filtr jest opcjonalny i tylko dodatkowo zawęża [ andWhere(...) ]
        return $this->json($reservationRepository->getByFilters($data['filters']));
    }
}
