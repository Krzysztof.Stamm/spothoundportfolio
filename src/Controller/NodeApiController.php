<?php

namespace App\Controller;

use App\Entity\Node;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/node', name: 'api_node_'), Tag(name:'Node')]
class NodeApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get node by ID",
     *     description="Returns node details by ID ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the node",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="name", type="string", example="Chair"),
     *             @OA\Property(property="zone", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="exampleZone")),
     *             @OA\Property(
     *                 property="configuration",
     *                 oneOf={
     *                     @OA\Schema(
     *                         type="object",
     *                         @OA\Property(property="color", type="string", example="Green")
     *                     ),
     *                     @OA\Schema(
     *                         type="array",
     *                         @OA\Items(type="string")
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Node not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Node $node
     */
    public function index(Node $node): JsonResponse
    {
        return $this->json($node);
    }
}
