<?php

namespace App\Controller;

use App\Entity\Account;
use App\Repository\AccountRepository;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/organization', name: 'api_organization_'), Tag(name: 'Organization')]
class OrganizationApiController extends AbstractController
{
    /**
     * @Route("", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get organization",
     *     description="Returns organization details",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="name", type="string", example="Great organization"),
     *             @OA\Property(property="owner", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="email", type="string", example="example@wp.pl"))
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organization not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     */
    public function index(): JsonResponse
    {
        $account = $this->getUser();

        assert($account instanceof Account);

        return $this->json($account->getOrganization());
    }

    /**
     * @Route("/accounts", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get accounts associated with current organization",
     *     description="Returns accounts associated with current organization",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                 @OA\Property(property="username", type="string", example="admin"),
     *                 @OA\Property(property="fullName", type="string", example="Adam Nowak"),
     *                 @OA\Property(property="accountType", type="string", example="TYPE_OWNER"),
     *             )
     *          )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param AccountRepository $accountRepository
     */
    public function getAccounts(AccountRepository $accountRepository): JsonResponse
    {
        $account = $this->getUser();

        assert($account instanceof Account);

        $accounts = $accountRepository->getAccountsForOrganization($account->getOrganization());

        return $this->json($accounts);
    }
}
