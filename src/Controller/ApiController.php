<?php

namespace App\Controller;

use App\Entity\Account;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api', name: 'api_'), Tag(name:'Util')]
class ApiController extends AbstractController
{
    #[Route('/test', name: 'test', methods: ['GET'])]
    public function index(): JsonResponse
    {
        return $this->json('Test');
    }

    #[Route('/send/email', name: 'send_email', methods: ['POST'])]
    public function sendEmail(MailerInterface $mailer): JsonResponse
    {
        $email = (new Email())
            ->to('foo.bar@gmail.com')
            ->subject('Test')
            ->text('Hello world')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        try {
            $mailer->send($email);
            $message = 'Email sent successfullys!';
        } catch (TransportExceptionInterface $e) {
            $message = 'Error!';
        }

        return $this->json($message);
    }

    /**
     * @Route("/me", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get current logged account informations",
     *     description="Returns the currently authenticated user",
     *     @OA\Response(
     *         response=200,
     *         description="The authenticated user",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="email", type="string", example="krzysztof.stamm@wp.pl")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     */
    public function me(): JsonResponse
    {
        $account = $this->getUser();

        if (!$account) {
            return new JsonResponse(['error' => 'User not found.'], Response::HTTP_NOT_FOUND);
        }
        assert($account instanceof Account);

        $place = $account->getPlaceContext();

        return new JsonResponse([
            'id' => $account->getId(),
            'email' => $account->getEmail(),
            'firstName' => $account->getFirstname(),
            'lastName' => $account->getLastname(),
            'place' => [
                'id' => $place != null ? $place->getId() : null,
                'name' => $place != null ? $place->getName() : null,
                'address' => $place != null ? $place->getAddress() : null
            ],
            'organization' => $account->getOrganization()
        ]);
    }
}
