<?php

namespace App\Controller;

use App\Entity\Customer;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/customer', name: 'api_customer_'),Tag(name:'Customer')]
class CustomerApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get customer by ID",
     *     description="Returns customer details by ID ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the customer",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="name", type="string", example="Adam"),
     *             @OA\Property(property="lastName", type="string", example="Adamowicz"),
     *             @OA\Property(property="phone", type="string", example="123123123"),
     *             @OA\Property(property="email", type="string", example="AA@wp.pl"),
     *             @OA\Property(property="tokens", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                      @OA\Property(property="validTo", type="string", format="date-time", example="2023-04-05T01:34:38+00:00"),
     *                      @OA\Property(property="usedAt", type="string", format="date-time", example="2023-04-01T01:34:43+00:00")
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Customer not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param Customer $customer
     */
    public function index(Customer $customer): JsonResponse
    {
        return $this->json($customer);
    }
}
