<?php

namespace App\Controller;

use App\Entity\CustomerToken;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

#[Route('/api/customerToken', name: 'api_customerToken_'), Tag(name:'Customer token')]
class CustomerTokenApiController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get customer token by ID",
     *     description="Returns customer token details by ID ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the customer token",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *             @OA\Property(property="validTo", type="string", format="date-time", example="2023-04-05T01:34:38+00:00"),
     *             @OA\Property(property="usedAt", type="string", format="date-time", example="2023-04-01T01:34:43+00:00"),
     *             @OA\Property(property="customer", type="object",
     *                  @OA\Property(property="id", type="string", example="5e84f84a-264e-46e8-8e09-ab2c6ea61eec"),
     *                  @OA\Property(property="name", type="string", example="Adam"),
     *                  @OA\Property(property="lastName", type="string", example="Adamowicz"),
     *                  @OA\Property(property="phone", type="string", example="123123123"),
     *                  @OA\Property(property="email", type="string", example="AA@wp.pl")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Customer token not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="JWT Token not found"
     *     )
     * )
     * @param CustomerToken $customerToken
     */
    public function index(CustomerToken $customerToken): JsonResponse
    {
        return $this->json($customerToken);
    }
}
