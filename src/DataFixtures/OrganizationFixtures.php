<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Organization;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class OrganizationFixtures extends Fixture
{
    public function __construct(
        private readonly UserPasswordHasherInterface $userPasswordHasher
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $account = (new Account())
            ->setAccountType(Account::TYPE_OWNER)
            ->setEmail('test@example.org')
            ->setFirstname('test_name')
            ->setLastname('test_lastname')
            ->setUsername('test')
            ->setRoles(['ROLE_USER'])
            ->setIsVerified(true);

        $account->setPassword(
            $this->userPasswordHasher->hashPassword(
                $account,
                'qwe123'
            )
        );

        $manager->persist($account);

        $organization = (new Organization())
            ->setOwner($account)
            ->setAlias('test')
            ->setName('test_org');

        $manager->persist($organization);
        $manager->flush();

        $this->addReference('test-organization', $organization);
    }
}
