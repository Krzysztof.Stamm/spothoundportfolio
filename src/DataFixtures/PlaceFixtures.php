<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlaceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $address = (new Address())
            ->setPostCode('00-000')
            ->setCity('test')
            ->setStreet('test')
            ->setCountry('test');

        $place = (new Place())
            ->setName('test_place')
            ->setType('test')
            ->setOrganization($this->getReference('test-organization'))
            ->setAddress($address);

        $manager->persist($place);
        $manager->flush();

        $this->addReference('test-place', $place);
    }

    public function getDependencies(): array
    {
        return [
            OrganizationFixtures::class,
        ];
    }
}
