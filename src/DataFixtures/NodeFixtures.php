<?php

namespace App\DataFixtures;

use App\Entity\Node;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class NodeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $node = (new Node())
            ->setName('test_node')
            ->setConfiguration([])
            ->setZone($this->getReference('test-zone'))
            ->setSize(4);

        $manager->persist($node);
        $manager->flush();

        $this->addReference('test-node', $node);
    }

    public function getDependencies(): array
    {
        return [
            ZoneFixtures::class,
        ];
    }
}
