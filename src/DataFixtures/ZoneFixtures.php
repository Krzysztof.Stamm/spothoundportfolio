<?php

namespace App\DataFixtures;

use App\Entity\Zone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ZoneFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $zone = (new Zone())
            ->setName('test')
            ->setPosition(1)
            ->setPlace($this->getReference('test-place'))
            ->setActive(true)
            ->setConfiguration([]);

        $manager->persist($zone);
        $manager->flush();

        $this->addReference('test-zone', $zone);
    }

    public function getDependencies(): array
    {
        return [
            PlaceFixtures::class,
        ];
    }
}
