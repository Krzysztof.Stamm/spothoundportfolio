<?php

namespace App\Enum;

enum ReservationStatus: int
{
    public static function fromName(string $name): int
    {
        foreach (self::cases() as $status) {
            if (mb_strtolower($name) === mb_strtolower($status->name)) {
                return $status->value;
            }
        }
        throw new \ValueError("$name is not a valid " . self::class);
    }

    public static function fromValue(int $value): string
    {
        foreach (self::cases() as $status) {
            if ($status->value === $value) {
                return $status->name;
            }
        }
        throw new \ValueError("$value is not a valid value for " . self::class);
    }

    /**
     * @return array<string, int>
     */
    public static function getChoices(): array
    {
        $choices = [];

        foreach (self::cases() as $status) {
            $choices[$status->name] = $status->value;
        }

        return $choices;
    }

    case RESERVED = 1;

    case ACTIVE = 2;

    case FINISHED = 3;

    case CANCELLED = 4;

    case DROPPED = 5;
}
