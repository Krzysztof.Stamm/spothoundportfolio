<?php

namespace App\Repository;

use App\Entity\Node;
use App\Entity\Zone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Types\UuidType;

/**
 * @extends ServiceEntityRepository<Node>
 */
class NodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Node::class);
    }

    /**
     * @param Zone $zone
     * @return array<int, array<string, bool|int|string>>
     */
    public function getNodesForZone(Zone $zone): array
    {
        return $this->createQueryBuilder('n')
            ->select('z.name as zone', 'n.id', 'n.name', 'n.size', 'n.configuration')
            ->join('n.zone', 'z')
            ->where('n.zone = :zone')
            ->setParameter('zone', $zone->getId(), UuidType::NAME)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getNodes(): array
    {
        return $this->createQueryBuilder('n')
            ->select('CAST(n.id as varchar) as id', 'n.name as node_name', 'z.name as zone_name', 'n.configuration')
            ->leftJoin('n.zone', 'z')
            ->getQuery()
            ->getResult();
    }
}
