<?php

namespace App\Repository;

use App\Entity\Place;
use App\Entity\Zone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Zone>
 */
class ZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zone::class);
    }

    /**
     * @param Place $place
     * @return array<int, array<string, bool|int|string>>
     */
    public function getZonesForPlace(Place $place): array
    {
        return $this->createQueryBuilder('z')
            ->select('z.id', 'z.name', 'z.position', 'z.active', 'z.configuration')
            ->join('z.place', 'p')
            ->where('z.place = :place')
            ->setParameter('place', $place)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getZones()
    {
        return $this->createQueryBuilder('z')
            ->select('CAST(z.id as varchar) as id', 'z.name as zone_name', 'p.name as place_name', 'z.configuration', 'z.position', 'z.active')
            ->leftJoin('z.place', 'p')
            ->getQuery()
            ->getResult();
    }
}
