<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\AccountPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AccountPlace>
 */
class AccountPlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountPlace::class);
    }

    /**
     * @param Account $account
     * @return array<int, array<string, bool|int|string>>
     */
    public function getPlacesForAccount(Account $account): array
    {
        return $this->createQueryBuilder('pa')
            ->select('p.id', 'pa.employedAs', 'p.name', 'p.type', 'a.street', 'a.city', 'a.postCode', 'a.country')
            ->join('pa.place', 'p')
            ->join('p.address', 'a')
            ->where('pa.account = :account')
            ->setParameter('account', $account)
            ->getQuery()
            ->getResult();
    }
}
