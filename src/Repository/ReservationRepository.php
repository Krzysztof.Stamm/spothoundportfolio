<?php

namespace App\Repository;

use App\Entity\Node;
use App\Entity\Place;
use App\Entity\Reservation;
use App\Enum\ReservationStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Types\UuidType;

/**
 * @extends ServiceEntityRepository<Reservation>
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function checkAvailability(Node $node, \DateTimeInterface $dateFrom, \DateTimeInterface $dateTo): bool
    {
        if ($dateFrom === $dateTo) {
            return false;
        }

        $qb = $this->createQueryBuilder('r');

        $res = $qb->select('COUNT(r.id) as cnt')
            ->where('r.node = :node')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->lt('r.dateFrom', ':dateTo'),
                        $qb->expr()->gt('r.dateTo', ':dateFrom'),
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->gte('r.dateFrom', ':dateFrom'),
                        $qb->expr()->lt('r.dateFrom', ':dateTo'),
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->gt('r.dateTo', ':dateFrom'),
                        $qb->expr()->lte('r.dateTo', ':dateTo'),
                    ),
                )
            )
            ->andWhere($qb->expr()->notIn('r.status', ':finalStatus'))
            ->setParameters([
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'finalStatus' => [
                    ReservationStatus::CANCELLED,
                    ReservationStatus::DROPPED,
                    ReservationStatus::FINISHED,
                ],
            ])
            ->setParameter('node', $node->getId(), UuidType::NAME)
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleScalarResult();

        return 0 === (int) $res;
    }

    /**
     * @param array<string, mixed> $filters
     * @return array<Reservation>
     */
    public function getByFilters(array $filters): array
    {
        if (!empty($filters['reservation'])) {
            return $this->findBy(['id' => $filters['reservation']]);
        }

        $qb = $this->createQueryBuilder('r');

        if (!empty($filters['dateFrom']) && empty($filters['dateTo'])) {
            if ($dateFrom = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $filters['dateFrom'])) {
                $filters['dateTo'] = $dateFrom->format('Y-m-d 23:59:59');
            }
        }

        if (!empty($filters['place']) || !empty($filters['zone'])) {
            $qb
                ->join('r.node', 'n')
                ->join('n.zone', 'z');
        }

        if (!empty($filters['place'])) {
            $qb
                ->join('z.place', 'p')
                ->andWhere($qb->expr()->eq('p', ':place'));
        }

        if (!empty($filters['dateFrom'])) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->lte('r.dateFrom', ':dateFrom'),
                        $qb->expr()->gte('r.dateTo', ':dateFrom'),
                    ),
                    $qb->expr()->gte('r.dateFrom', ':dateFrom'),
                )
            );
        }

        if (!empty($filters['dateTo'])) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->lte('r.dateFrom', ':dateTo'),
                        $qb->expr()->gte('r.dateTo', ':dateTo'),
                    ),
                    $qb->expr()->lte('r.dateTo', ':dateTo'),
                )
            );
        }

        if (!empty($filters['zone'])) {
            $qb->andWhere($qb->expr()->eq('z.id', ':zone'));
        }

        if (!empty($filters['customer'])) {
            $qb->andWhere($qb->expr()->like('LOWER(r.customerDetails)', ':customer'));
            $filters['customer'] = '%' . $filters['customer'] . '%';
        }

        return $qb->setParameters($filters)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ?Place $place
     * @param \DateTimeInterface $dateFrom
     * @param \DateTimeInterface $dateTo
     * @return array<Reservation>
     */
    public function getReservationsForPlace(?Place $place, \DateTimeInterface $dateFrom, \DateTimeInterface $dateTo): array
    {
        $qb = $this->createQueryBuilder('r');

        return $qb
            ->select('r')
            ->join('r.node', 'n')
            ->join('n.zone', 'z')
            ->join('z.place', 'p', 'WITH', $qb->expr()->eq('p', ':place'))
            ->andWhere(
                $qb->expr()->eq('p', ':place'),
                $qb->expr()->notIn('r.status', ':finalStatus'),
                $qb->expr()->between('r.dateFrom', ':dateFrom', ':dateTo'),
            )
            ->setParameters([
                'place' => $place,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'finalStatus' => [
                    ReservationStatus::CANCELLED,
                    ReservationStatus::DROPPED,
                    ReservationStatus::FINISHED,
                ],
            ])
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    public function save(Reservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Reservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
