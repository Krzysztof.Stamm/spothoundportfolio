<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\Organization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<Account>
 */
class AccountRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function save(Account $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Account $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param PasswordAuthenticatedUserInterface $user
     * @param string $newHashedPassword
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof Account) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->save($user, true);
    }

    /**
     * @param ?Organization $organization
     * @return array<int, array<string, string>>
     */
    public function getAccountsForOrganization(?Organization $organization): array
    {
        if (null === $organization) {
            return [];
        }

        $qb = $this->createQueryBuilder('a');

        return $qb->select(['a.id', 'a.username', "CONCAT(a.firstname, ' ', a.lastname) as fullName", 'a.accountType'])
            ->andWhere('a.organization = :org')
            ->setParameter('org', $organization)
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * @return array<int, array<string, string>>
     */
    public function getAccounts()
    {
        return $this->createQueryBuilder('a')
            ->select('CAST(a.id as varchar) as id', 'o.name as organizationName', 'pc.name as placeContextName', 'a.email', 'a.username', 'a.firstname', 'a.lastname', 'a.accountType', 'a.isVerified')
            ->leftJoin('a.organization', 'o')
            ->leftJoin('a.placeContext', 'pc')
            ->getQuery()
            ->getResult();
    }
}
