<?php

namespace App\Repository;

use App\Entity\Node;
use App\Entity\Place;
use App\Entity\Zone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Place>
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function getNodeCount(?Place $place): int
    {
        $qb = $this->createQueryBuilder('p');

        try {
            return $qb
                ->select('COUNT(DISTINCT n.id)')
                ->join(Zone::class, 'z', 'WITH', $qb->expr()->eq('z.place', ':place'))
                ->join(Node::class, 'n', 'WITH', $qb->expr()->eq('n.zone', 'z'))
                ->setParameters([
                    'place' => $place,

                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception) {
            return 0;
        }
    }
}
