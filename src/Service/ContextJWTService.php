<?php

namespace App\Service;

use App\Entity\Place;
use App\Repository\PlaceRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ContextJWTService
{
    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly JWTTokenManagerInterface $jwtManager,
        private readonly PlaceRepository $placeRepository
    ) {
    }

    public function getPlaceContext(): ?Place
    {
        $decodedJwtToken = $this->getDecodedToken();

        if (empty($decodedJwtToken)) {
            return null;
        }

        return $this->placeRepository->find($decodedJwtToken['place'] ?? '');
    }

    /**
     * @return array<string, string>
     */
    public function getDecodedToken(): array
    {
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            return [];
        }

        try {
            $decodedJwtToken = $this->jwtManager->decode($token);
        } catch (\Throwable) {
            $decodedJwtToken = [];
        }

        return $decodedJwtToken ?: [];
    }
}
