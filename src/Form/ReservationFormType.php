<?php

namespace App\Form;

use App\Entity\Node;
use App\Entity\Reservation;
use App\Enum\ReservationStatus;
use App\Repository\NodeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReservationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('node', EntityType::class, [
                'class' => Node::class,
                'query_builder' => function (NodeRepository $nodeRepository) {
                    $nodeRepository->findAll();
                },
                'choice_label' => function (Node $node) {
                    return $node->getName();
                },
            ])
            ->add('status', ChoiceType::class, [
                'choices' => ReservationStatus::getChoices(),
            ])
            ->add('dateFrom', DateTimeType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a start of reservation.',
                    ]),
                ]
            ])
            ->add('dateTo', DateTimeType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an end of reservation.',
                    ]),
                ]
            ])
            ->add('size', IntegerType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a size.',
                    ]),
                ],
            ])
            ->add('note', TextType::class)
            ->add('details', TextType::class, [
                'property_path' => 'ownerDetails'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Reservation::class,
        ]);
    }
}
