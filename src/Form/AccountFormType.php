<?php

namespace App\Form;

use App\Entity\Account;

use App\Entity\Organization;
use App\Repository\OrganizationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class AccountFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email([
                        'message' => 'The email {{ value }} is not a valid email.',
                    ]),
                ],
            ])
            ->add('username', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an username.',
                    ]),
                ],
            ])
            ->add('organization', EntityType::class, [
                'class' => Organization::class,

                'query_builder' => function (OrganizationRepository $organizationRepository) {
                    $organizationRepository->findAll();
                },
                'choice_label' => function (Organization $organization) {
                    return $organization->getName();
                },
            ])
            ->add('accountType', ChoiceType::class, [
//                'label' => false,
                'choices' => Account::ACCOUNT_TYPES,
                'required' => true,
            ])
            ->add('isVerified', ChoiceType::class, [
//                'label' => false,
                'choices' => ['true' => true, 'false' => false],
                'required' => true,
            ])
            ->add('firstname', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an firstName.',
                    ]),
                ],
            ])
            ->add('lastname', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an lastName.',
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Account::class,
        ]);
    }
}
