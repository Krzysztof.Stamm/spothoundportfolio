<?php

namespace App\Form;

use App\Entity\Account;

use App\Entity\Organization;
use App\Repository\AccountRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrganizationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a name.',
                    ]),
                ],
            ])
            ->add('alias', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an alias.',
                    ]),
                ],
            ])
            ->add('owner', EntityType::class, [
                'class' => Account::class,

                'query_builder' => function (AccountRepository $accountRepository) {
                    $accountRepository->findAll();
                },
                'choice_label' => function (Account $account) {
                    return $account->getFullName();
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Organization::class,
        ]);
    }
}
