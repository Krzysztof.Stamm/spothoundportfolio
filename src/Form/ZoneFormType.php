<?php

namespace App\Form;

use App\Entity\Place;
use App\Entity\Zone;
use App\Repository\PlaceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints\NotBlank;

class ZoneFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('place', EntityType::class, [
                'class' => Place::class,

                'query_builder' => function (PlaceRepository $placeRepository) {
                    $placeRepository->findAll();
                },
                'choice_label' => function (Place $place) {
                    return $place->getName();
                },
            ])
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a name.',
                    ]),
                ],
            ])
            ->add('position', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a position.',
                    ]),
                ],
            ])
            ->add('active', ChoiceType::class, [
                'choices' => ['true' => true, 'false' => false]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Zone::class,
        ]);
    }
}
