<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationFailureListener
{
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event): void
    {
        $exception = $event->getException();

        if ($exception->getCode() === 2100) {
            $response = new JWTAuthenticationFailureResponse($exception->getMessage(), Response::HTTP_UNAUTHORIZED);
            $event->setResponse($response);
        }
    }
}
