<?php

namespace App\EventListener;

use App\Entity\Account;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener
{
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();

        /** @var Account $account */
        $account = $event->getUser();

        $payload['place'] = $account->getPlaceContext()?->getId();
        $payload['organization'] = $account->getOrganization()?->getId();

        $event->setData($payload);
    }
}
