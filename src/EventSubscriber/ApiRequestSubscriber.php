<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiRequestSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['deserializeRequest', 0],
        ];
    }

    public function deserializeRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ($request->getContentTypeFormat() === 'json') {
            $content = $request->getContent();

            if (empty($content)) {
                return;
            }

            $data = json_decode($content, true, flags: JSON_THROW_ON_ERROR);
            $request->request = new InputBag($data);
        }
    }
}
