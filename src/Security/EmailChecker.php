<?php

namespace App\Security;

use App\Entity\Account;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class EmailChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof Account) {
            return;
        }

        if (!$user->isVerified()) {
            throw new AuthenticationException('Verify your email first before trying to log in.', 2100);
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        // nic
    }
}
