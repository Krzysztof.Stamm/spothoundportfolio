<?php

namespace App\AdminController;

use App\Entity\Account;
use App\Form\AccountFormType;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/account', name: 'admin_account_')]
class AccountController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(AccountRepository $accountRepository): Response
    {
        return $this->render('admin/account/index.html.twig', [
            'accounts' => $accountRepository->getAccounts()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Account $account, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(AccountFormType::class, $account, [
        'action' => $this->generateUrl('admin_account_edit', ['id' => $account->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This account has been edited successfully.');

            return $this->redirectToRoute('admin_account_index');
        }

        return $this->render('admin/account/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Account $account, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        try {
            $em->remove($account);
        } catch (\Exception) {
            $type = 'danger';
            $message = 'Something go wrong!';
        }
        $em->flush();
        $flasher->addFlash($type ?? 'success', $message ?? 'This account has been deleted successfully.');

        return $this->redirectToRoute('admin_account_index');
    }
}
