<?php

namespace App\AdminController;

use App\Entity\Node;
use App\Form\NodeFormType;
use App\Repository\NodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/node', name: 'admin_node_')]
class NodeController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(NodeRepository $nodeRepository): Response
    {
        return $this->render('admin/node/index.html.twig', [
            'nodes' => $nodeRepository->getNodes()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Node $node, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(NodeFormType::class, $node, [
            'action' => $this->generateUrl('admin_node_edit', ['id' => $node->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This node has been edited successfully.');

            return $this->redirectToRoute('admin_node_index');
        }

        return $this->render('admin/node/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Node $node, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        try {
            $em->remove($node);
        } catch (\Exception) {
            $type = 'danger';
            $message = 'Something go wrong!';
        }
        $em->flush();
        $flasher->addFlash($type ?? 'success', $message ?? 'This node has been deleted successfully.');

        return $this->redirectToRoute('admin_node_index');
    }
}
