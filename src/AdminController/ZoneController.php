<?php

namespace App\AdminController;

use App\Entity\Zone;
use App\Form\ZoneFormType;
use App\Repository\ZoneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/zone', name: 'admin_zone_')]
class ZoneController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(ZoneRepository $zoneRepository): Response
    {
        return $this->render('admin/zone/index.html.twig', [
            'zones' => $zoneRepository->getZones()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Zone $zone, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(ZoneFormType::class, $zone, [
            'action' => $this->generateUrl('admin_zone_edit', ['id' => $zone->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This zone has been edited successfully.');

            return $this->redirectToRoute('admin_zone_index');
        }

        return $this->render('admin/zone/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Zone $zone, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        try {
            $em->remove($zone);
        } catch (\Exception) {
            $type = 'danger';
            $message = 'Something go wrong!';
        }
        $em->flush();
        $flasher->addFlash($type ?? 'success', $message ?? 'This zone has been deleted successfully.');

        return $this->redirectToRoute('admin_zone_index');
    }
}
