<?php

namespace App\AdminController;

use App\Entity\Place;
use App\Form\PlaceFormType;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/place', name: 'admin_place_')]
class PlaceController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(PlaceRepository $placeRepository): Response
    {
        return $this->render('admin/place/index.html.twig', [
            'places' => $placeRepository->findAll()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Place $place, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(PlaceFormType::class, $place, [
            'action' => $this->generateUrl('admin_place_edit', ['id' => $place->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This place has been edited successfully.');

            return $this->redirectToRoute('admin_place_index');
        }

        return $this->render('admin/place/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
