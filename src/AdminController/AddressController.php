<?php

namespace App\AdminController;

use App\Entity\Address;
use App\Form\AddressFormType;
use App\Repository\AddressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/address', name: 'admin_address_')]
class AddressController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(AddressRepository $addressRepository): Response
    {
        return $this->render('admin/address/index.html.twig', [
            'addresses' => $addressRepository->getAddresses()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Address $address, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(AddressFormType::class, $address, ['action' => $this->generateUrl('admin_address_edit', ['id' => $address->getId()])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This address has been edited successfully.');

            return $this->redirectToRoute('admin_address_index');
        }

        return $this->render('admin/address/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Address $address, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        try {
            $em->remove($address);
        } catch (\Exception) {
            $type = 'danger';
            $message = 'Something go wrong!';
        }
        $em->flush();
        $flasher->addFlash($type ?? 'success', $message ?? 'This address has been deleted successfully.');

        return $this->redirectToRoute('admin_address_index');
    }
}
