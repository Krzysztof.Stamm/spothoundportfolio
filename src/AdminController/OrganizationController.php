<?php

namespace App\AdminController;

use App\Entity\Organization;
use App\Form\OrganizationFormType;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/organization', name: 'admin_organization_')]
class OrganizationController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(OrganizationRepository $organizationRepository): Response
    {
        return $this->render('admin/organization/index.html.twig', [
            'organizations' => $organizationRepository->findAll()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Organization $organization, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(OrganizationFormType::class, $organization, [
            'action' => $this->generateUrl('admin_organization_edit', ['id' => $organization->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This organization has been edited successfully.');

            return $this->redirectToRoute('admin_organization_index');
        }

        return $this->render('admin/organization/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
