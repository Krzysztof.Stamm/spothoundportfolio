<?php

namespace App\AdminController;

use App\Entity\Customer;
use App\Form\CustomerFormType;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/customer', name: 'admin_customer_')]
class CustomerController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(CustomerRepository $customerRepository): Response
    {
        return $this->render('admin/customer/index.html.twig', [
            'customers' => $customerRepository->getCustomers()
        ]);
    }

    #[Route('/{id}/edit', name: 'edit')]
    public function edit(Customer $customer, Request $request, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        $form = $this->createForm(CustomerFormType::class, $customer, [
            'action' => $this->generateUrl('admin_customer_edit', ['id' => $customer->getId()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->flush();
            } catch (\Exception) {
                $type = 'danger';
                $message = 'Something go wrong!';
            }

            $flasher->addFlash($type ?? 'success', $message ?? 'This customer has been edited successfully.');

            return $this->redirectToRoute('admin_customer_index');
        }

        return $this->render('admin/customer/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/delete', name: 'delete')]
    public function delete(Customer $customer, EntityManagerInterface $em, FlasherInterface $flasher): Response
    {
        try {
            $em->remove($customer);
        } catch (\Exception) {
            $type = 'danger';
            $message = 'Something go wrong!';
        }
        $em->flush();
        $flasher->addFlash($type ?? 'success', $message ?? 'This customer has been deleted successfully.');

        return $this->redirectToRoute('admin_customer_index');
    }
}
