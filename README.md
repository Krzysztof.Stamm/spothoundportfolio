# Spothound-backend

## Odpalanie projektu na Windowsie

W pierwszej kolejności należy zainstalować WSL2.
Najlepiej wybrać Ubuntu jako system operacyjny.

1. Otwieramy konsolę poleceniem: `wsl`.
2. Przechodzimy do katalogu domowego użytkownika: `cd ~`.
3. Pobieramy projekt z repo: `git clone https://gitlab.com/spothound/spothound-backend.git`.
4. Przechodzimy do katalogu: `cd spothound-backend`.
5. Przygotowujemy docker-compose.yml: `cp docker-compose.windows.yml docker-compose.yml`.
6. Tworzymy sieć dockerową: docker network create spothound. (może wyrzucić błąd, jeżeli sieć już istnieje)
7. Odpalamy projekt poleceniem `docker-compose up` lub `docker-compose up -d` (jeśli chcemy odpalić w tle).
8. Do pliku `c:\Windows\System32\Drivers\etc\hosts` dodajemy `127.0.0.1 spothound.local`.
9. `cp .env .env.local`
10. W pliku `.env.local` zmieniamy adres bazy na `spothound-database:5432`
11. Przechodzimy do powłoki kontenera php `docker-compose exec spothound-php bash`
12. Instalujemy niezbędne pakiety `composer install npm install`
13. Tworzymy bazę danych `php bin/console doctrine:database:create`
14. Wykonujemy migracje `php bin/console doctrine:migrations:migrate`
15. Aplikacja dostępna jest pod adresem `spothound.local:1945`



## Przydatne informacje

### Dostęp do kontenera

`docker-compose exec nazwa_kontenera bash`

W naszym przypadku będziemy głównie korzystać w kontenera, w którym znajduje się php, dlatego użyjemy polecenia:

`docker-compose exec spothound-php bash`

### Inne

Należy pamiętać aby nie korzystać z lokalnego phpa do operacji na projekcie.  
Wszystkie komendy np. `composer require`, `php bin/console` wykonujemy z poziomu kontenera.
