import '../assets/css/material-dashboard.scss';
import './styles/app.css';
import './styles/select2.css';
import "bootstrap";
import "select2/dist/js/select2.min";
import "select2/dist/css/select2.min.css";

global.$ = global.jQuery = $;

$(function() {
    $('#modal').modal()
    $('[data-bs-toggle="tooltip"]').tooltip()
    $('#modal').on('shown.bs.modal', function () {
        $('.select2').select2();
    });

    $("ul.nav li").click(function() {
        $("ul.nav li").removeClass("active");
        $(this).addClass("active");
        localStorage.setItem("activeElement", $(this).index());
    });

    const activeIndex = localStorage.getItem("activeElement");
    if (activeIndex !== null) {
        $("ul.nav li").eq(activeIndex).addClass("active");
    }
});
