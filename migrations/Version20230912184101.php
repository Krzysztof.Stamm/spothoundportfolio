<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230912184101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE node ADD size INT NOT NULL DEFAULT 2');
        $this->addSql('ALTER TABLE reservation RENAME COLUMN owner_details TO customer_details');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE reservation RENAME COLUMN customer_details TO owner_details');
        $this->addSql('ALTER TABLE node DROP size');
    }
}
