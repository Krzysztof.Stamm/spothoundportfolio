<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230507161932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE refresh_token_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE account (id UUID NOT NULL, organization_id UUID DEFAULT NULL, place_context_id UUID DEFAULT NULL, email VARCHAR(180) DEFAULT NULL, username VARCHAR(50) NOT NULL, firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, account_type VARCHAR(50) NOT NULL, is_verified BOOLEAN DEFAULT false NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A4E7927C74 ON account (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A4F85E0677 ON account (username)');
        $this->addSql('CREATE INDEX IDX_7D3656A432C8A3DE ON account (organization_id)');
        $this->addSql('CREATE INDEX IDX_7D3656A445C057C7 ON account (place_context_id)');
        $this->addSql('COMMENT ON COLUMN account.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN account.organization_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN account.place_context_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE account_place (id UUID NOT NULL, place_id UUID NOT NULL, account_id UUID NOT NULL, employed_as VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_42928ABCDA6A219 ON account_place (place_id)');
        $this->addSql('CREATE INDEX IDX_42928ABC9B6B5FBA ON account_place (account_id)');
        $this->addSql('COMMENT ON COLUMN account_place.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN account_place.place_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN account_place.account_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE address (id UUID NOT NULL, post_code VARCHAR(10) NOT NULL, city VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, country VARCHAR(100) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN address.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE customer (id UUID NOT NULL, name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone VARCHAR(32) NOT NULL, email VARCHAR(255) DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN customer.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE customer_token (id UUID NOT NULL, customer_id UUID DEFAULT NULL, token VARCHAR(255) NOT NULL, valid_to TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, used_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9102D4009395C3F3 ON customer_token (customer_id)');
        $this->addSql('COMMENT ON COLUMN customer_token.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN customer_token.customer_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN customer_token.valid_to IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN customer_token.used_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE node (id UUID NOT NULL, zone_id UUID NOT NULL, name VARCHAR(255) NOT NULL, configuration JSON NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_857FE8459F2C3FAB ON node (zone_id)');
        $this->addSql('COMMENT ON COLUMN node.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN node.zone_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE organization (id UUID NOT NULL, owner_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(10) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C1EE637C7E3C61F9 ON organization (owner_id)');
        $this->addSql('COMMENT ON COLUMN organization.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN organization.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE place (id UUID NOT NULL, address_id UUID NOT NULL, organization_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_741D53CDF5B7AF75 ON place (address_id)');
        $this->addSql('CREATE INDEX IDX_741D53CD32C8A3DE ON place (organization_id)');
        $this->addSql('COMMENT ON COLUMN place.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN place.address_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN place.organization_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE refresh_token (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C74F2195C74F2195 ON refresh_token (refresh_token)');
        $this->addSql('CREATE TABLE zone (id UUID NOT NULL, place_id UUID NOT NULL, name VARCHAR(255) NOT NULL, position INT NOT NULL, active BOOLEAN NOT NULL, configuration JSON NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A0EBC007DA6A219 ON zone (place_id)');
        $this->addSql('COMMENT ON COLUMN zone.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN zone.place_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A432C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A445C057C7 FOREIGN KEY (place_context_id) REFERENCES place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_place ADD CONSTRAINT FK_42928ABCDA6A219 FOREIGN KEY (place_id) REFERENCES place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_place ADD CONSTRAINT FK_42928ABC9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE customer_token ADD CONSTRAINT FK_9102D4009395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE node ADD CONSTRAINT FK_857FE8459F2C3FAB FOREIGN KEY (zone_id) REFERENCES zone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE zone ADD CONSTRAINT FK_A0EBC007DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE refresh_token_id_seq CASCADE');
        $this->addSql('ALTER TABLE account DROP CONSTRAINT FK_7D3656A432C8A3DE');
        $this->addSql('ALTER TABLE account DROP CONSTRAINT FK_7D3656A445C057C7');
        $this->addSql('ALTER TABLE account_place DROP CONSTRAINT FK_42928ABCDA6A219');
        $this->addSql('ALTER TABLE account_place DROP CONSTRAINT FK_42928ABC9B6B5FBA');
        $this->addSql('ALTER TABLE customer_token DROP CONSTRAINT FK_9102D4009395C3F3');
        $this->addSql('ALTER TABLE node DROP CONSTRAINT FK_857FE8459F2C3FAB');
        $this->addSql('ALTER TABLE organization DROP CONSTRAINT FK_C1EE637C7E3C61F9');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CDF5B7AF75');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CD32C8A3DE');
        $this->addSql('ALTER TABLE zone DROP CONSTRAINT FK_A0EBC007DA6A219');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE account_place');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE customer_token');
        $this->addSql('DROP TABLE node');
        $this->addSql('DROP TABLE organization');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE refresh_token');
        $this->addSql('DROP TABLE zone');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
