#!/bin/sh

set -e

if [ "${1#-}" != "$1" ]; then
  set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ] || [ "$1" = 'bin/console' ]; then
  if [ -f composer.json ]; then
    composer install --prefer-dist --no-progress --no-interaction
  fi

  if grep -q "lexik/jwt" composer.json; then
    php bin/console lexik:jwt:generate-keypair --skip-if-exists
  fi

  if grep -q ^DATABASE_URL= .env; then
    ATTEMPTS_LEFT_TO_REACH_DATABASE=60
    until [ $ATTEMPTS_LEFT_TO_REACH_DATABASE -eq 0 ] || DATABASE_ERROR=$(php bin/console dbal:run-sql "SELECT 1" 2>&1); do
      if [ $? -eq 255 ]; then
        ATTEMPTS_LEFT_TO_REACH_DATABASE=0
        break
      fi
      sleep 1
      ATTEMPTS_LEFT_TO_REACH_DATABASE=$((ATTEMPTS_LEFT_TO_REACH_DATABASE - 1))
      echo "Still waiting for db to be ready... Or maybe the db is not reachable. $ATTEMPTS_LEFT_TO_REACH_DATABASE attempts left"
    done

    if [ $ATTEMPTS_LEFT_TO_REACH_DATABASE -eq 0 ]; then
      echo "The database is not up or not reachable:"
      echo "$DATABASE_ERROR"
      exit 1
    else
      echo "The db is now ready and reachable"
    fi

    if [ "$(find ./migrations -iname '*.php' -print -quit)" ]; then
      php bin/console doctrine:migrations:migrate --no-interaction
    fi
  fi
fi

exec docker-php-entrypoint "$@"
