<?php

namespace App\Tests\Reservation;

use App\DataFixtures\NodeFixtures;
use App\Entity\Node;
use App\Entity\Reservation;
use App\Enum\ReservationStatus;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReservationCollisionTest extends KernelTestCase
{
    protected AbstractDatabaseTool $databaseTool;
    protected EntityManagerInterface $em;

    public const TEST_DATE_FROM = '2023-11-11 12:15:00';
    public const TEST_DATE_TO = '2023-11-11 13:00:00';

    //                     12:15           13:00
    // ----------------------|-------R-------|----------------------
    //    |----C1----|
    //            |----C2----|
    //                |----C3----|
    //                |----------C4----------|
    //                |----------------C5------------|
    //                       |----C6-----|
    // ----------------------|-------R-------|----------------------
    //                       |------C7-------|
    //                       |----------C8-----------|
    //                          |---C9---|
    //                          |----C10-----|
    //                          |--------C11--------|
    //                                       |----C12----|
    //                                           |----C13----|
    // ----------------------|-------R-------|----------------------
    //                     12:15           13:00

    public const CASES = [[
            'dateFrom' => '2023-11-11 10:00:00',
            'dateTo' => '2023-11-11 11:30:00',
            'expectedResult' => true,
        ], [
            'dateFrom' => '2023-11-11 11:15:00',
            'dateTo' => '2023-11-11 12:15:00',
            'expectedResult' => true,
        ], [
            'dateFrom' => '2023-11-11 11:30:00',
            'dateTo' => '2023-11-11 12:30:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 11:30:00',
            'dateTo' => '2023-11-11 13:00:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 11:30:00',
            'dateTo' => '2023-11-11 13:30:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:15:00',
            'dateTo' => '2023-11-11 12:45:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:15:00',
            'dateTo' => '2023-11-11 13:00:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:15:00',
            'dateTo' => '2023-11-11 13:30:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:30:00',
            'dateTo' => '2023-11-11 12:45:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:30:00',
            'dateTo' => '2023-11-11 13:00:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 12:30:00',
            'dateTo' => '2023-11-11 13:30:00',
            'expectedResult' => false,
        ], [
            'dateFrom' => '2023-11-11 13:00:00',
            'dateTo' => '2023-11-11 13:30:00',
            'expectedResult' => true,
        ], [
            'dateFrom' => '2023-11-11 13:15:00',
            'dateTo' => '2023-11-11 13:45:00',
            'expectedResult' => true,
        ],
    ];


    /**
     * @after
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->databaseTool);
        $this->em->close();
    }

    /**
     * @before
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $this->em = static::getContainer()->get(EntityManagerInterface::class);

        $this->databaseTool->loadFixtures([
            NodeFixtures::class
        ]);

        $node = $this->em->getRepository(Node::class)->findOneBy(['name' => 'test_node']);

        assert($node instanceof Node);

        $reservation = (new Reservation())
            ->setNode($node)
            ->setNote('test_note')
            ->setSize(2)
            ->setCustomerDetails('customer_details')
            ->setDateFrom(new \DateTimeImmutable(self::TEST_DATE_FROM))
            ->setDateTo(new \DateTimeImmutable(self::TEST_DATE_TO))
            ->setStatus(ReservationStatus::RESERVED->value);

        $this->em->persist($reservation);
        $this->em->flush();
    }

    /**
     * @test
     */
    public function testCollisionDetections(): void
    {
        /** @var Node $node */
        $node = $this->em->getRepository(Node::class)->findOneBy(['name' => 'test_node']);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->em->getRepository(Reservation::class);

        foreach (self::CASES as $case) {
            $available = $reservationRepository->checkAvailability(
                $node,
                new \DateTimeImmutable($case['dateFrom']),
                new \DateTimeImmutable($case['dateTo'])
            );

            $this->assertSame($available, $case['expectedResult']);
        }
    }
}
